export interface Movie {
    id?: number;
    tconst: string;
    titleType: string;
    primaryTitle: string;
    originalTitle: string;
    isAdult: number;
    startYear: number;
    endYear: number;
    runtimeMinutes: string;
    genres: string
}
