import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  apiUrl = "http://localhost:5000/movies";
  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<Movie[]>(this.apiUrl);
  }
  
  delete(id: any) {
      return this.http.delete(`${this.apiUrl}/${id}`);
  }

  persist(movie: Movie) {
    return this.http.post<Movie>(this.apiUrl, movie);
  }

  update(movie: Movie) {
    return this.http.put(`${this.apiUrl}/${movie.id}`, movie); 
  }
}
