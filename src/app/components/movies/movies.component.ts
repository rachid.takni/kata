import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  
  editForm = false;

  showForm = false;

  searchText = "";
  
  page = 0;
  pageSize = 10;

  myMovie: Movie = {
    tconst: "",
    titleType: "",
    primaryTitle: '',
    originalTitle: "",
    isAdult: 0,
    startYear: 0,
    endYear: 0,
    runtimeMinutes: "",
    genres: ''
  }

  movies: Movie[] = [];

  resultMovies: Movie[] = [];

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies() {
    this.movieService.findAll()
          .subscribe(movies => {
            this.resultMovies = this.movies = movies;
          })
          
  }

  deleteMovie(id: any) {
    this.movieService.delete(id)
        .subscribe(() => {
          this.resultMovies = this.movies = this.movies.filter(movie => movie.id != id)
        })
  }

  persistMovie() {
    this.movieService.persist(this.myMovie)
          .subscribe((movie) => {
            this.resultMovies = this.movies = [movie, ...this.movies];
            this.resetMovie();
            this.showForm = false;
          })
  }

  resetMovie() {
    this.myMovie = {
      tconst: "",
      titleType: "",
      primaryTitle: '',
      originalTitle: "",
      isAdult: 0,
      startYear: 0,
      endYear: 0,
      runtimeMinutes: "",
      genres: ''
    }
  }

  editMovie(movie: Movie) {
    this.myMovie = movie;
    this.editForm = true;
    this.showForm = true;
  }

  updateMovie() {
    this.movieService.update(this.myMovie)
          .subscribe((movie) => {
            this.resetMovie();
            this.editForm = false
          })
  }

  searchMovies() {
    this.resultMovies = this.movies.filter(movie => movie.primaryTitle.toLowerCase().includes(this.searchText.toLowerCase()) )
  }

}
